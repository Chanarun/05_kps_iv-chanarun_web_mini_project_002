import React, { useState } from "react";
import { Container, Form, Button, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { postCategory } from "../service/categoryService";

export default function PostArticle() {
  const [name, setName] = useState("");

  const createCategory = async (e) => {
    e.preventDefault();
    let category = { name };

    postCategory(category).then((message) => {
      console.log(message);
      Swal.fire({
        icon: "success",
        title: message,
        showConfirmButton: false,
        timer: 1500,
      });
    });
  };
  return (
    <Container>
      <h1>Add new category</h1>
      <Button variant="success" size="small" as={Link} to="/category">
        Back
      </Button>
      <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Title</Form.Label>
          <Form.Control
            type="text"
            placeholder="Title"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </Form.Group>
        <Button
          className="mt-2 me-2"
          variant="primary"
          type="submit"
          onClick={createCategory}
        >
          Submit
        </Button>
      </Form>
    </Container>
  );
}
