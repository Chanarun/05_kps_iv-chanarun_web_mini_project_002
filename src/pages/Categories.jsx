import { Button } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import { Card, Col, Container, Row, Nav } from "react-bootstrap";
import getAllArticles, { deleteArticle } from "../service/articleService";
import Swal from "sweetalert2";
import { useNavigate, Link } from "react-router-dom";
import getAllCategory from "../service/categoryService";

export default function Category() {
  const [categories, setCategories] = useState([]);
  let navigateca = useNavigate();

  useEffect(() => {
    let fetch = async () => {
      let response = await getAllCategory();
      setCategories(response);
    };
    fetch();
  }, []);

  const onDeleteCategory = (id) => {
    Swal.fire({
      title: "Are you sure to delete?",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: "Yes",
      denyButtonText: `No`,
    }).then((result) => {
      if (result.isConfirmed) {
        let newArticles = categories.filter((article) => article._id !== id);
        setCategories(newArticles);
        deleteArticle(id).then((response) =>
          Swal.fire(response, "", "success")
        );
      } else if (result.isDenied) {
        Swal.fire("Category is not deleted", "", "info");
      }
    });
  };
  let categoryCard = categories.map((category, index) => {
    return (
      <Col className="col-3 mt-3 rounded-100">
        <Card>
          <Card.Body>
            <Card.Title className="text-center texttitle-dots">
              {category.name}
            </Card.Title>
            <Button
              className="w-100 mt-1"
              variant="primary"
              onClick={() => navigateca(`/category/${category._id}`)}
            >
              View
            </Button>
            <Button
              className="w-100 mt-1"
              variant="warning"
              size="small"
              as={Link}
              to="/postCategory"
            >
              Edit
            </Button>
            <Button
              className="w-100 mt-1"
              variant="danger"
              onClick={() => onDeleteCategory(category._id)}
            >
              Delete
            </Button>
          </Card.Body>
        </Card>
      </Col>
    );
  });
  return (
    <Container>
      <h1>All Category</h1>
      <Button variant="secondary" size="small" as={Link} to="/postCategory">
        New Categories
      </Button>

      <Row>{categoryCard}</Row>
    </Container>
  );
}
