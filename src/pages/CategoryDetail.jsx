import React, { useEffect, useState } from "react";
import { Container, Card, Nav, Button } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import { viewCategoryById } from "../service/categoryService";

export default function CategoryDetail() {
  const [category, setCategory] = useState({});
  const { id } = useParams();

  useEffect(() => {
    viewCategoryById(id).then((response) => {
      console.log(response);
      setCategory(response);
    });
  }, []);

  return (
    <Container>
      <Button
        className="mt-2 me-auto"
        variant="success"
        type="submit"
        as={Link}
        to="/category"
      >
        Back
      </Button>
      <h1>Category Name: {category?.name}</h1>
    </Container>
  );
}
