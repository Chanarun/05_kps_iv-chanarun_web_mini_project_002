import { Button } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import { Card, Col, Container, Row, Nav } from "react-bootstrap";
import getAllArticles, { deleteArticle } from "../service/articleService";
import Swal from "sweetalert2";
import { useNavigate, Link } from "react-router-dom";

export default function Article() {
  const [articles, setArticles] = useState([]);
  let navigate = useNavigate();

  useEffect(() => {
    let fetch = async () => {
      let response = await getAllArticles();
      setArticles(response);
    };
    fetch();
  }, []);

  const onDeleteArticle = (id) => {
    Swal.fire({
      title: "Are you sure to delete?",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: "Yes",
      denyButtonText: `No`,
    }).then((result) => {
      if (result.isConfirmed) {
        let newArticles = articles.filter((article) => article._id !== id);
        setArticles(newArticles);
        deleteArticle(id).then((response) =>
          Swal.fire(response, "", "success")
        );
      } else if (result.isDenied) {
        Swal.fire("Article is not deleted", "", "info");
      }
    });
  };
  let articleCard = articles.map((article, index) => {
    return (
      <Col className="col-2 mt-3 rounded-100">
        <Card>
          <Card.Img
            className="rounded "
            variant="top"
            src={article.image}
            style={{ objectFit: "cover", height: "150px" }}
          />
          <Card.Body>
            <Card.Title className="text-center texttitle-dots">
              {article.title}
            </Card.Title>
            <Card.Text className="text-center textdes-dots">
              {article.description}
            </Card.Text>
            <Button
              className="w-100 mt-1"
              variant="primary"
              onClick={() => navigate(`/article/${article._id}`)}
            >
              View
            </Button>
            <Button
              className="w-100 mt-1"
              variant="warning"
              size="small"
              as={Link}
              to="/post"
            >
              Edit
            </Button>
            <Button
              className="w-100 mt-1"
              variant="danger"
              onClick={() => onDeleteArticle(article._id)}
            >
              Delete
            </Button>
          </Card.Body>
        </Card>
      </Col>
    );
  });
  return (
    <Container>
      <h1>All Article</h1>

      <Button variant="secondary" size="small" as={Link} to="/post">
        New Article
      </Button>

      <Row>{articleCard}</Row>
    </Container>
  );
}
