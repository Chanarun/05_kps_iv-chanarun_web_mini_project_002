import React, { useState } from "react";
import { Container, Form, Button, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { postArticle, uploadImage } from "../service/articleService";

export default function PostArticle() {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [imgUrl, setImgUrl] = useState(
    "https://cwimports.com.au/wp-content/uploads/2020/10/no-image.png"
  );
  const [imgFile, setImgFile] = useState(null);

  const createArticle = async (e) => {
    e.preventDefault();
    let article = { title, description };

    if (imgFile) {
      let url = await uploadImage(imgFile);
      article.image = url;
    }

    postArticle(article).then((message) => {
      console.log(message);
      Swal.fire({
        icon: "success",
        title: message,
        showConfirmButton: false,
        timer: 1500,
      });
    });
  };
  return (
    <Container>
      <h1>Add New Article</h1>
      <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Title</Form.Label>
          <Form.Control
            type="text"
            placeholder="Title"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Description</Form.Label>
          <Form.Control
            type="text"
            placeholder="Description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formCheckbox">
          <Form.Check type="checkbox" label="Is Published" />
        </Form.Group>
        <img
          src={
            imgUrl ??
            "https://cwimports.com.au/wp-content/uploads/2020/10/no-image.png"
          }
          alt=""
          className="w-25"
        />
        <br></br>
        <Form.Label>Choose image</Form.Label>

        <Form.Control
          type="file"
          onChange={(e) => {
            console.log(e.target.files[0]);
            let url = URL.createObjectURL(e.target.files[0]);
            setImgFile(e.target.files[0]);
            setImgUrl(url);
          }}
        />

        <Button
          className="mt-2 me-2"
          variant="primary"
          type="submit"
          onClick={createArticle}
        >
          Submit
        </Button>
        <Button
          className="mt-2 me-auto"
          variant="danger"
          type="submit"
          as={Link}
          to="/"
        >
          Cancel
        </Button>
      </Form>
    </Container>
  );
}
