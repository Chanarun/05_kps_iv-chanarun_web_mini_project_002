import React, { useEffect, useState } from "react";
import { Container, Card, Nav, Button } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import { viewArticleById } from "../service/articleService";

export default function ArticleDetail() {
  const [article, setArticle] = useState({});
  const { id } = useParams();

  useEffect(() => {
    viewArticleById(id).then((response) => {
      console.log(response);
      setArticle(response);
    });
  }, []);

  return (
    <Container>
      <Button
        className="mt-2 me-auto"
        variant="success"
        type="submit"
        as={Link}
        to="/"
      >
        Back
      </Button>
      <h1>Title: {article?.title}</h1>
      <h4>Description :{article?.description}</h4>
      <Card.Img className="rounded w-50" variant="top" src={article.image} />
    </Container>
  );
}
