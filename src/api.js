import axios from "axios";

const api = axios.create({
    baseURL: "https://ams.hrd-edu.info/api"
})

export default api