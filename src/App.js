import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Article from './pages/Home';
import NavbarMenu from './components/NavbarMenu';
import { Route, Routes } from 'react-router-dom';
import PostArticle from './pages/PostArticle';
import ArticleDetail from './pages/ArticleDetail';
import Categories from './pages/Categories';
import PostCategory from './pages/PostCategory';
import CategoryDetail from './pages/CategoryDetail';

function App() {

  return (
   <div>
     <NavbarMenu />
     <Routes>
      <Route path='/' element ={<Article />} />
      <Route path='/post' element={<PostArticle />} />
      <Route path='/Category' element={<Categories />} />
      <Route path='/article/:id' element={<ArticleDetail />} />   
      <Route path='/category/:id' element={<CategoryDetail />} /> 
      <Route path='/postCategory' element={<PostCategory />} />  
     </Routes>
   </div>
    
  );
}

export default App;
