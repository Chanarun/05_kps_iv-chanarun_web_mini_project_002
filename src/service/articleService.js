import api from "../api";

// Get all articles
export const getAllArticles = async () => {
    let response = await api.get('/articles')
    // console.log(response);
    return response.data.payload
}

// Post article
export const postArticle= async (article) =>{
    let response =await api.post('/articles', article)
    return response.data.message
}

// Upload image
export const uploadImage= async(file) =>{
    let formData=new FormData();
    formData.append('image',file)
    // console.log(formData);
    let response = await api.post('/images',formData)
    console.log(response.data.payload.url);
    return response.data.payload.url
}

// Delete article
export const deleteArticle = async (id) =>{
    let response = await api.delete(`/articles/${id}`)
    console.log(response);
    return response.data.message
}

// Get article by id
export const viewArticleById =async (id) =>{
    let response =await api.get(`/articles/${id}`)
    return response.data.payload
}

export default getAllArticles
