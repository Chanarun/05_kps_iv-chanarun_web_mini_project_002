import api from "../api";

// Get all articles
export const getAllCategory = async () => {
    let response = await api.get('/category')
    // console.log(response);
    return response.data.payload
}

// Post category
export const postCategory= async (category) => {
    let response =await api.post('/category', category)
    return response.data.message
}


// Delete category
export const deleteCategory = async (id) =>{
    let response = await api.delete(`/category/${id}`)
    console.log(response);
    return response.data.message
}

// Get category by id
export const viewCategoryById =async (id) =>{
    let response =await api.get(`/category/${id}`)
    return response.data.payload
}

export default getAllCategory;